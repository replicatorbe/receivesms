/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ReceiveSMS;


import android.content.BroadcastReceiver;
import android.os.Bundle;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.widget.Toast;

/**
 *
 * @author Jérôme Fafchamps <jerome@fafchamps.be>
 */
public class PokeReceiver extends BroadcastReceiver {

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {
            // récupérer SMS
            Bundle bundle = intent.getExtras();        
            SmsMessage[] msgs = null;
          
            if (bundle != null)
            {
                // récupérer le SMS
                Object[] pdus = (Object[]) bundle.get("pdus");
                msgs = new SmsMessage[pdus.length];     
                String body ="";
                for (int i=0; i<msgs.length; i++){
                    msgs[i] = SmsMessage.createFromPdu((byte[])pdus[i]);                
                    body =  msgs[i].getMessageBody().toString();  
                    if(body.startsWith("#test#")){
                                       // action a effectuer à la réception du SMS: ici affichage du mot POKE
                                       // lorsque le SMS commence par ##androPoke##
                        Toast.makeText(context, "POKE", Toast.LENGTH_SHORT).show();
               
                        smsToHttp sms = new smsToHttp();
                        sms.send(body.toString());
                    }
                    Toast.makeText(context, body, Toast.LENGTH_SHORT).show();
                }
                
            }
                        
        }
    }

}
